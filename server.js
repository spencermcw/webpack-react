const path = require("path");
const express = require("express");
const webpack = require("webpack");
const config = require("./webpack.config.js");

const app = express();
const compiler = webpack(config);

var DIST_DIR = path.join(__dirname, "dist");
var PORT = 3000;

app.use(require("webpack-dev-middleware")(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));

app.use(require("webpack-hot-middleware")(compiler));

app.get("*", function(req, res) {
  res.sendFile(path.join(DIST_DIR, "index.html"));
});

app.listen(PORT, 'localhost', err => {
  if (err) {
    console.log(err);
    return;
  }

  console.log("Listening at http://localhost:%s", PORT);
});